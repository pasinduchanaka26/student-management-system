<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'faculty_id', 'department','is_active','is_deleted'
    ];

    public function getFaculty(){
        return $this->belongsTo('App\Faculty', 'faculty_id');
    }
}
