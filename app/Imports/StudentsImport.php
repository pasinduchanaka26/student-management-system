<?php

namespace App\Imports;

use App\Student;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Support\Facades\Validator;

class StudentsImport implements
    ToCollection,
    WithHeadingRow


{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $logedUser = Auth::user();
        Validator::make($rows->toArray(), [
            '*.reg_no'=>['email','unique:users']
        ])->validate();
        try{
            foreach ($rows as $row) {
           //dd($rows);
           $user=User::create([
               'email'=>$row['reg_no'],
               'role'=>6,
               'password'=>Hash::make($row['password']),
               'name'=>$row['reg_no'],
           ]);
           $student=Student::create([
               'user_id'=>$user->id,
               'first_name'=>$row['first_name'],
               'last_name'=>$row['last_name'],
               'registration_number'=>$row['reg_no'],
               'email'=>$row['email'],
               'batch_id'=>$row['batch'],
               'is_deleted'=>0,
               'is_active'=>1,
               'added_by'=>$logedUser->id,
               'profile_picture'=>null,

           ]);

       }
           }catch(\Maatwebsite\Excel\Validators\ValidationException $e){

              /* //throw $e;
               $failures = $e->failures();
               foreach ($failures as $failure) {
                   $failure->row(); // row that went wrong
                   $failure->attribute(); // either heading key (if using heading row concern) or column index
                   $failure->errors(); // Actual error messages from Laravel validator
                   $failure->values(); // The values of the row that has failed.
               }*/
           }

    }


}
