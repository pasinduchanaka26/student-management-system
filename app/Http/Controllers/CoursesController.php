<?php

namespace App\Http\Controllers;

use App\Courses;
use App\DegreeProgram;
use App\Department;
use App\Semester;
use App\Year;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $year=Year::where('is_active',1)
            ->where('is_deleted',0)
            ->get();

        $semester=Semester::where('is_active',1)
            ->where('is_deleted',0)
            ->get();

        $departments=Department::where('is_active',1)
            ->where('is_deleted',0)
            ->get();

        return view('admin.courses.courses-add',compact('year','semester','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses=Courses::where('is_deleted',0)->get();
        return view('admin.courses.courses-view',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'course' => ['required'],
            'course_code' => ['required'],
            'year' => ['required'],
            'semester' => ['required'],
            'credits' => ['required'],
            'department_id' => ['required'],
            'degree_program_id' => ['required'],
        ]);

        $course=new Courses();
        $course->is_active="1";
        $course->is_deleted="0";
        $course->fill($request->all());

        if($course->save()){
            return back()->with('info', 'Course successfully added');
        }else{
            return back()->with('error', 'Something went wrong !!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $courses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function edit($courses)
    {
        $year=Year::where('is_active',1)
            ->where('is_deleted',0)
            ->get();

        $semester=Semester::where('is_active',1)
            ->where('is_deleted',0)
            ->get();

        $departments=Department::where('is_active',1)
            ->where('is_deleted',0)
            ->get();

        $courses=Courses::where('id',$courses)
            ->first();

        return view('admin.courses.courses-update',compact('courses','year','semester','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$courses)
    {
        $courses=Courses::findOrFail($courses);
        if($courses->update($request->all())){
            return back()->with('info', 'Course successfully updated');
        }else{
            return back()->with('error', 'Something went wrong !!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function destroy($courses)
    {
        $courses=Courses::findOrFail($courses);
        $courses->is_deleted=1;
        if($courses->update()){
            return response()->json(['status' => 'true']);
        }else{
            return response()->json(['status' => 'false']);
        }
    }
    public function getSemesters(){

        $semesters=Semester::where('is_active',1)
            ->where('is_deleted',0)
            ->get();

        return response()->json(['status' => 'true','semesters'=>$semesters]);
    }
    public function getDegreeProgram($id){

        $degreePrograms=DegreeProgram::where('is_active',1)
            ->where('is_deleted',0)
            ->where('department_id',$id)
            ->get();

        return response()->json(['status' => 'true','degreePrograms'=>$degreePrograms]);
    }
}
