<?php

namespace App\Http\Controllers;

use App\DegreeProgram;
use App\Department;
use Illuminate\Http\Request;



class DegreeProgramController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $department=Department::where('is_active',1)
            ->where('is_deleted',0)
            ->get();
        return view('admin.degree-program.degree-program-add',compact('department'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $degreeProgrms=DegreeProgram::where('is_deleted',0)->get();
        return view('admin.degree-program.degree-program-view',compact('degreeProgrms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $degreeProgram=new DegreeProgram();
        $degreeProgram->added_user="0";
        $degreeProgram->alias="0";
        $degreeProgram->is_active="1";
        $degreeProgram->is_deleted="0";
        $degreeProgram->fill($request->all());

        if($degreeProgram->save()){
            return back()->with('info', 'Degree Program successfully added');
        }else{
            return back()->with('error', 'Something went wrong !!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DegreeProgram  $degreeProgram
     * @return \Illuminate\Http\Response
     */
    public function show(DegreeProgram $degreeProgram)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DegreeProgram  $degreeProgram
     * @return \Illuminate\Http\Response
     */
    public function edit(DegreeProgram $degreeProgram)
    {
        $departments=Department::where('is_active',1)->get();
        return view('admin.degree-program.degree-program-update',compact('degreeProgram','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DegreeProgram  $degreeProgram
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$degreeProgram)
    {
        $degreeProgram=DegreeProgram::findOrFail($degreeProgram);
        if($degreeProgram->update($request->all())){
            return back()->with('info', 'Degree Program successfully updated');
        }else{
            return back()->with('error', 'Something went wrong !!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DegreeProgram  $degreeProgram
     * @return \Illuminate\Http\Response
     */
    public function destroy($degreeProgram)
    {
        $degreeProgram=DegreeProgram::findOrFail($degreeProgram);
        $degreeProgram->is_deleted=1;
        if($degreeProgram->update()){
            return response()->json(['status' => 'true']);
        }else{
            return response()->json(['status' => 'false']);
        }
    }
}
