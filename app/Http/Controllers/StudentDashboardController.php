<?php

namespace App\Http\Controllers;

use App\Department;
use App\Roll;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $studentDetails=Student::where('is_deleted',0)
            ->where('user_id',Auth::user()->id)
            ->first();
        $departements=Department::where('is_deleted',0)->get();
        if ($studentDetails->profile_completed_stage==0){
            //return view('student.self-reg',compact('studentDetails','departements'));
            $view = 'student.layouts.app';
            return view('auth.passwords.password-change',compact('view'));
        }else{
            return view('student.student-dashboard');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'degree_id' => ['required'],
            'name_with_initial' => ['required'],
            'department_id' => ['required'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'full_name' => ['required'],
            'nationality' => ['required'],
            'race' => ['required'],
            'religion' => ['required'],
            'birth_date' => ['required'],
            'nic' => ['required'],
            'contact_address' => ['required'],
            'permanent_address' => ['required'],
            'province' => ['required'],
            'district' => ['required'],
            'mobile_phone' => ['required'],
            'land_phone' => ['required'],
            'guardian_name' => ['required'],
            'guardian_telephone' => ['required'],
            'guardian_address' => ['required'],
        ]);
        try {
            $db_transaction=DB::transaction(function ()use($request) {

                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('profile_pictures'), $imageName);

                $student=Student::find($request->id);
                $student->profile_picture=$imageName;
                $data=$request->all();
                $student->update($data);
            });
            //info variable is not working with return view method.
            //return view('student.student-dashboard')->with('info', 'Student successfully registered!!');
            return redirect()->route('student-dashboard.index')->with('info', 'Student successfully registered!!');

        }catch(\Illuminate\Database\QueryException $e){
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=Roll::find(Auth::user()->role);
        if($role->roll == 'Admin') {
            $view = 'admin.layouts.app';
        } elseif($role->roll == 'Students') {
            $view = 'student.layouts.app';
        }
        $student=Student::findOrFail($id);
        $departements=Department::where('is_deleted',0)->get();

        return view('student.student-profile',compact('student','departements','view'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'image' => '|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'degree_id' => ['required'],
            'name_with_initial' => ['required'],
            'department_id' => ['required'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'full_name' => ['required'],
            'nationality' => ['required'],
            'race' => ['required'],
            'religion' => ['required'],
            'birth_date' => ['required'],
            'nic' => ['required'],
            'contact_address' => ['required'],
            'permanent_address' => ['required'],
            'province' => ['required'],
            'district' => ['required'],
            'mobile_phone' => ['required'],
            'land_phone' => ['required'],
            'guardian_name' => ['required'],
            'guardian_telephone' => ['required'],
            'guardian_address' => ['required'],
        ]);
        try {
            $db_transaction=DB::transaction(function ()use($request,$id) {
                $student=Student::find($id);
                if(isset($request->image)){
                    $imageName = time().'.'.$request->image->extension();
                    $request->image->move(public_path('profile_pictures'), $imageName);
                    $student->profile_picture=$imageName;
                }
                $data=$request->all();
                $student->update($data);
            });
            return redirect()->back()->with('info', 'Student successfully updated!!');

        }catch(\Illuminate\Database\QueryException $e){
            $this->rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStudentProfileStatus($profile_completed_stage,$id){
        $student=Student::find($id);
        $student->profile_completed_stage=$profile_completed_stage;
        $student->update();
    }

}
