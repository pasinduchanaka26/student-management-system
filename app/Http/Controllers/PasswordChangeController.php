<?php

namespace App\Http\Controllers;


use App\Roll;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\StudentDashboardController;

class PasswordChangeController extends Controller
{
    public function __construct()

    {
        $this->middleware('auth');
    }
    public function index()
    {
        $role=Roll::find(Auth::user()->role);

        if($role->roll == "Admin") {
            $view = 'admin.layouts.app';
        } elseif($role->roll == "Students") {
            $view = 'student.layouts.app';
        }

        return view('auth.passwords.password-change',compact('view'));

    }
    public function store(Request $request)
    {
        $request->validate([

            'current_password' => ['required'],

            'new_password' => ['required'],

            'new_confirm_password' => ['same:new_password'],

        ]);
        $role=Roll::find(Auth::user()->role);
        if ($request->new_password){
            $user=Auth::user();
            if ($request->current_password){
                if (Hash::check($request->current_password, $user->password)){

                    if ($request->new_password == $request->new_confirm_password){
                        User::find(Auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
                        if($role->roll == "Students") {
                            $student=Student::where('user_id',Auth()->user()->id)->first();
                            if( $student->profile_completed_stage==0){
                                $studentDashboardController=new StudentDashboardController();
                                //1=first time password changed
                                $studentDashboardController->changeStudentProfileStatus(1,$student->id);
                            }


                        }
                        Session::flash('info', 'Password updated');
                        return redirect()->back();
                    }else{
                        Session::flash('confirm_password', 'Confirm Password Does not match.');
                        return redirect()->back();
                    }
                }else{
                    Session::flash('current_password', 'Current Password Does not match');
                    return redirect()->back();
                }
            }

        }

    }

}
