<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Imports\StudentsImport;
use App\Notifications\StudentRegistration;
use App\Student;
use App\User;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $batch=Batch::where('is_deleted',0)
            ->where('is_active',1)
            ->get();
        return view('admin.student.student-quick-registration',compact('batch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $batch=Batch::where('is_deleted',0)
            ->where('is_active',1)
            ->get();
        return view('admin.student.student-view',compact('batch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'registration_number' => ['required'],
            'password' => ['required'],
            'batch_id' => ['required'],
            'email' => ['required'],
        ]);
        try {
            $db_transaction=DB::transaction(function ()use($request) {

                $logedUser = Auth::user();
                $user=new User();
                $user->email=$request->registration_number;
                $user->role=6;
                $user->password=Hash::make($request->password);
                $user->name=$request->registration_number;
                $user->save();

                $student=new Student();
                $student->user_id= $user->id;
                $student->batch_id= $request->batch_id;
                $student->is_deleted=0;
                $student->is_active=1;
                $student->profile_picture=null;
                $student->added_by= $logedUser->id;
                $student->email=$request->email;
                $student->registration_number=$request->registration_number;
                $student->save();

                return  $student;
            });
            $name=$db_transaction->registration_number;
            $password=$request->password;
            $when = now()->addSecond(5);
            Notification::route('mail', $request->email)
                ->route('nexmo', '5555555555')
                ->route('slack', 'https://hooks.slack.com/services/...')
                ->notify((new StudentRegistration($name))
                ->delay($when));


            return back()->with('info', 'Student successfully added');

        } catch(\Illuminate\Database\QueryException $e){
            //throw $e;
            $errorCode = $e->errorInfo[1];
            if($errorCode == '1062'){
                return back()->with('duplicte', 'Duplicate entry');
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }

    public function getStudents($id)
    {
        if($id==0){
            $students=DB::table('students')
                ->Leftjoin('departments', 'students.department_id', '=', 'departments.id')
                ->select('students.*', 'departments.department')
                ->where('students.is_deleted',0)
                ->where('students.is_active',1)
                ->get();

        }else{
            $students=DB::table('students')
                ->join('departments', 'students.department_id', '=', 'departments.id')
                ->select('students.*', 'departments.department')
                ->where('students.is_deleted',0)
                ->where('students.is_active',1)
                ->where('batch_id',$id)
                ->get();
        }

        return response()->json(['status' => 'true','students'=>$students]);
    }
    public function addstudentBulkView()
    {
        return view('admin.student.add-student-bulk');
    }
    public function addstudentBulk(Request $request)
    {
        ///dd ($request->file());
        $logedUser = Auth::user();
        $this->validate($request, [
            'student_bulk'  => 'required|mimes:xls,xlsx'
        ]);

            $file=$request->file('student_bulk');
            //Excel::import(new StudentsImport, $file);
            $import=new StudentsImport();
            $import->import($file);

        return redirect()->back()->with('info', 'Excel Data Imported successfully.');
    }


}
