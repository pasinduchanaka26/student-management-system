<?php

namespace App\Http\Controllers;

use App\Roll;
use App\StaffDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rolls=Roll::where('is_active',1)
            ->where('is_deleted',0)
            ->where('is_staff',1)
            ->get();
        return view('admin.users.users-add',compact('rolls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$users=User::where('is_deleted','0')->get();
        $users=StaffDetail::where('staff_details.is_deleted','0')
            ->select('staff_details.*','rolls.roll','users.id AS userId')
            ->join('users', 'users.id', '=', 'staff_details.user_id')
            ->join('rolls', 'rolls.id', '=', 'users.role')
            ->get();
        return view('admin.users.users-view',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'password' => ['required'],
            'confirm_password' => ['same:password'],
            'mobile_phone' => ['required'],
            'email' => ['required'],
            'role' => ['required'],
        ]);
        if ($request->password==$request->confirm_password){
            try{
                $db_transaction=DB::transaction(function ()use($request) {

                    $logedUser = Auth::user();
                    $user=new User();
                    $user->email=$request->email;
                    $user->role=$request->role;
                    $user->password=Hash::make($request->password);
                    $user->name=$request->first_name." ".$request->last_name;
                    $user->save();

                    $staff=new StaffDetail();
                    $staff->user_id= $user->id;
                    $staff->first_name= $request->first_name;
                    $staff->last_name= $request->last_name;
                    $staff->mobile_phone= $request->mobile_phone;
                    $staff->land_phone= null;
                    $staff->is_deleted=0;
                    $staff->is_active=1;
                    $staff->added_user= $logedUser->id;
                    $staff->email=$request->email;
                    $staff->save();

                });
                /* $name=$db_transaction->registration_number;
                 $password=$request->password;
                 $when = now()->addSecond(5);
                 Notification::route('mail', $request->email)
                     ->route('nexmo', '5555555555')
                     ->route('slack', 'https://hooks.slack.com/services/...')
                     ->notify((new StudentRegistration($name))
                         ->delay($when));*/


                return redirect()->back()->with('info', 'User successfully added');

            }catch (\Illuminate\Database\QueryException $e){

                $errorCode = $e->errorInfo[1];
                if($errorCode == '1062'){
                    return redirect()->back()->with('duplicte', 'Duplicate entry');
                }
                throw $e;
            }

        }else{
            //Session::flash('password', 'Password Does not match');
            return redirect()->back()->with('not_match', 'Password Does not match');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rolls=Roll::where('is_active',1)
            ->where('is_deleted',0)
            ->where('is_staff',1)
            ->get();
        $users=StaffDetail::select('staff_details.*','users.*')
            ->join('users', 'users.id', '=', 'staff_details.user_id')
            ->where('users.id',$id)
            ->first();

        //return $users;
        return view('admin.users.users-update',compact('rolls','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
//            'password' => ['required'],
//            'confirm_password' => ['same:password'],
            'mobile_phone' => ['required'],
            'email' => ['required'],
            'role' => ['required'],
            'is_active' => ['required'],
        ]);
        try{
            $update=DB::transaction(function ()use($request,$id) {
                $user=User::findOrFail($id);

                $staffDetails=StaffDetail::where('user_id',$user->id)->first();

                $user->name=$request->first_name." ".$request->first_name;
                $user->role=$request->role;
                $user->update();

                $staffDetails->first_name=$request->first_name;
                $staffDetails->last_name=$request->last_name;
                $staffDetails->mobile_phone=$request->mobile_phone;
                $staffDetails->is_active=$request->is_active;
                $staffDetails->update();

                return true;

            });
        }catch(\Illuminate\Database\QueryException $e){
            $this->rollBack();
            throw $e;
        }

        if($update){
            return redirect()->back()->with('info', 'User successfully updated!!');
        }else{
            return redirect()->back()->with('error', 'Something went wrong !!');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users=StaffDetail::where('user_id',$id)->first();
        $users->is_deleted=1;
        if($users->update()){
            return response()->json(['status' => 'true']);
        }else{
            return response()->json(['status' => 'false']);
        }
    }
}
