<?php

namespace App\Http\Controllers;

use App\Department;
use App\Faculty;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $faculty=Faculty::where('is_active',1)->get();
        return view('admin.department.department-add',compact('faculty'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departements=Department::where('is_deleted',0)->get();
        return view('admin.department.department-view',compact('departements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department=new Department();
        $department->added_user="0";
        $department->is_active="1";
        $department->is_deleted="0";
        $department->fill($request->all());

        if($department->save()){
            return back()->with('info', 'Department successfully added');
        }else{
            return back()->with('error', 'Something went wrong !!');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        $faculty=Faculty::where('is_active',1)->get();
        return view('admin.department.department-update',compact('department','faculty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$department)
    {
        $department=Department::findOrFail($department);
        if($department->update($request->all())){
            return back()->with('info', 'Department successfully updated');
        }else{
            return back()->with('error', 'Something went wrong !!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($department)
    {
        $department=Department::findOrFail($department);
        $department->is_deleted=1;
        if($department->update()){
            return response()->json(['status' => 'true']);
        }else{
            return response()->json(['status' => 'false']);
        }

    }
}
