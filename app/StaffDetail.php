<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffDetail extends Model
{
    protected $fillable = [
        'first_name', 'last_name','mobile_phone','land_phone','email','added_user','is_active','is_deleted'
    ];

}
