<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'user_id', 'batch_id','department_id','degree_id','registration_number','name_with_initial','first_name','last_name',
        'full_name','nationality','race','religion','birth_date','nic','contact_address','permanent_address','district','province',
        'mobile_phone','land_phone','profile_picture','email','guardian_name','guardian_telephone','guardian_address','added_by','is_active','is_deleted',
        'profile_completed_stage',
    ];
    public function getDepartment(){
        return $this->belongsTo('App\Department', 'department_id');
    }
    public function getDegreeprogram(){
        return $this->belongsTo('App\DegreeProgram', 'degree_id');
    }
    public function getBatch(){
        return $this->belongsTo('App\Batch', 'batch_id');
    }
}
