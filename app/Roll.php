<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roll extends Model
{
    protected $fillable = [
        'roll', 'is_active','is_deleted','added_user'
    ];
}
