<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $fillable = [
        'course', 'course_code','year','semester','credits','department_id','degree_program_id','is_active','is_deleted'
    ];
    public function getYear(){
        return $this->belongsTo('App\Year', 'year');
    }
    public function getSemester(){
        return $this->belongsTo('App\Semester', 'semester');
    }
    public function getDepartment(){
        return $this->belongsTo('App\Department', 'department_id');
    }
    public function getDegreeprogram(){
        return $this->belongsTo('App\DegreeProgram', 'degree_program_id');
    }
}
