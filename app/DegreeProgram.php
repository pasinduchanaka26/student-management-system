<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DegreeProgram extends Model
{
    protected $fillable = [
        'department_id', 'degree_program','is_active','is_deleted'
    ];
    public function getDepartment(){
        return $this->belongsTo('App\Department', 'department_id');
    }
}
