<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('department', 'DepartmentController');
Route::resource('degree-program', 'DegreeProgramController');
Route::resource('courses', 'CoursesController');
Route::resource('student', 'StudentController');
Route::get('get-students/{id}', 'StudentController@getStudents');
Route::resource('student-dashboard', 'StudentDashboardController');

Route::get('password-change', 'PasswordChangeController@index')->name('password-change');
Route::post('password-change', 'PasswordChangeController@store')->name('password-change.store');

Route::get('get-semesters', 'CoursesController@getSemesters');
Route::get('get-degree-program/{id}', 'CoursesController@getDegreeProgram');

Route::resource('users','UserController');

Route::get('add-student-bulk', 'StudentController@addstudentBulkView')->name('add-student-bulk');
Route::post('student-bulk-import', 'StudentController@addstudentBulk')->name('student-bulk-import');
