@extends($view)

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Password Change</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Password Change</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-40" action="{{Route('password-change.store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label text-right col-md-12">Current Password</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <input type="password" id="current_password" name="current_password" class="form-control" placeholder="Current Password" required>
                                    {{--<small class="form-control-feedback"> This field has error. </small>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label text-right col-md-12">New Password</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <input type="password" id="new_password" name="new_password" class="form-control" placeholder="New Password" required>
                                    {{--<small class="form-control-feedback"> This field has error. </small>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label text-right col-md-12">Confirm Password</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <input type="password" id="new_confirm_password" name="new_confirm_password" class="form-control" placeholder="Confirm Password" required>
                                    {{--<small class="form-control-feedback"> This field has error. </small>--}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn waves-effect waves-light btn-primary">Update</button>

                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <!-- /.row -->
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Password successfully updated");
        </script>

    @elseif(session()->has('current_password'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Current password incorrect !!',
            })
        </script>
    @elseif(session()->has('confirm_password'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Password not match!!',
            })
        </script>
    @endif
@endsection
@section('script')
    <script>
        $(document).ready(function(){

            /* $id=$("#department_id option:selected").val();
             loadDegreeProgram($id);*/

            //load degree programme
            function loadDegreeProgram($id){
                $.ajax(
                    {
                        url: "/get-degree-program/"+$id,
                        type: 'GET',
                        dataType: "JSON",
                        data: {

                        },
                        success: function (response)
                        {
                            console.log(response);
                            if(response['status']=="true"){
                                $.each(response.degreePrograms, function (){
                                    $("#degree_id").append($("<option     />").val(this.id).text(this.degree_program));
                                });
                            }

                        }
                    });
            }

            $("#department_id").change(function(){
                $('#degree_id').empty();
                $id=$("#department_id option:selected").val();
                loadDegreeProgram($id);

            });
        });
    </script>
@endsection
