@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Update Degree Program</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Degree Program</li>
                <li class="breadcrumb-item active">Edit Degree Program</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-40" action="{{Route('degree-program.update',$degreeProgram->id)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="example-email">Degree Program</label>
                        <input type="text" id="degree_program" name="degree_program" value="{{$degreeProgram->degree_program}}" class="form-control" placeholder="Degree Program" required>
                    </div>
                    <div class="form-group">
                        <label>Department</label>
                        <select name="department_id" id="department_id" class="form-control" readonly="true">
                            @foreach($departments as $data)
                                <option value="{{$data->id}}" @php if($data->id==$degreeProgram->department_id)echo "selected" @endphp >{{$data->department}}</option>
                            @endforeach
                        </select>
                        {{--                        <input type="text" id="faculty" value="{{$faculty->faculty}}" name="faculty_id" class="form-control" placeholder="Faculty" required disabled>--}}
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="is_active" id="is_active" class="form-control">
                            <option value="1" @php if($degreeProgram->is_active==1)echo "selected" @endphp >Active</option>
                            <option value="0" @php if($degreeProgram->is_active==0)echo "selected" @endphp >De-Active</option>

                        </select>
                        {{--                        <input type="text" id="faculty" value="{{$faculty->faculty}}" name="faculty_id" class="form-control" placeholder="Faculty" required disabled>--}}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn waves-effect waves-light btn-primary">Update</button>
                        <a href="{{Route('degree-program.create')}}" class="btn waves-effect waves-light btn-success">Degree Program list</a>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Degree Program successfully updated");
        </script>

    @elseif(session()->has('error'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        </script>
    @endif
@endsection
