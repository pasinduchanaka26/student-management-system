@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">View Departments</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Degree Program</li>
                <li class="breadcrumb-item active">View Degree Program</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Degree Program</th>
                                <th>Department</th>
                                <th>Status</th>
                                <th>Added Date & Time</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($degreeProgrms as $degreeProgrm )
                                <tr>
                                    <td>{{$degreeProgrm->degree_program}}</td>
                                    <td>{{$degreeProgrm->getDepartment['department']}}</td>
                                    <td>
                                        @if($degreeProgrm->is_active==1)
                                            <div class="label label-table label-success">Active</div>
                                        @else
                                            <div class="label label-table label-danger">De-Active</div>
                                        @endif
                                    </td>
                                    <td>{{$degreeProgrm->created_at}}</td>
                                    <td>
                                        <a class="btn waves-effect waves-light btn-sm btn-info" href={{ Route('degree-program.edit',$degreeProgrm->id) }}><i class="ti-settings"></i></a>
                                        <button data-id="{{$degreeProgrm->id}}" data-token="{{ csrf_token() }}" class="btn waves-effect waves-light btn-sm btn-danger btnDelete"><i class="ti-trash"></i></button>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script>
        $(function () {
            $('#myTable').DataTable();
            $(function () {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function (settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function (group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function () {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        //delete button click

        $(document).ready(function(){
            $(".btnDelete").click(function(){
                //Warning Message

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {

                        var id = $(this).data("id");
                        var token = $(this).data("token");

                        $.ajax(
                            {
                                url: "/degree-program/"+id,
                                type: 'DELETE',
                                dataType: "JSON",
                                data: {
                                    "id": id,
                                    "_method": 'DELETE',
                                    "_token": token,
                                },
                                success: function (response)
                                {
                                    console.log(response);
                                    if(response['status']=="true"){
                                        Swal.fire(
                                            'Deleted!',
                                            'Department has been deleted.',
                                            'success'
                                        )
                                        setTimeout(function(){
                                            location.reload();
                                        }, 2000)
                                    }else{
                                        Swal.fire({
                                            type: 'error',
                                            title: 'Oops...',
                                            text: 'Something went wrong!',
                                        })
                                    }

                                }
                            });



                    }
                })

            });
        });

    </script>
@endsection
