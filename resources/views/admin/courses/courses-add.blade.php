@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Add Courses</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Courses</li>
                <li class="breadcrumb-item active">Add Courses</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-40" action="{{Route('courses.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="example-email">Course Name</label>
                        <input type="text" id="course" name="course" class="form-control" placeholder="Course Name" required>
                    </div>
                    <div class="form-group">
                        <label for="example-email">Course Code</label>
                        <input type="text" id="course_code" name="course_code" class="form-control" placeholder="Course Code" required>
                    </div>
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Year</label>
                                <select name="year" id="year" class="form-control" required>
                                      @foreach($year as $data)
                                          <option value="{{$data->id}}" @php if($data->year==now()->year)echo "selected" @endphp>{{$data->year}}</option>
                                      @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Semester</label>
                                <select name="semester" id="semester" class="form-control" required>
                                    {{--  @foreach($faculty as $data)
                                          <option value="{{$data->id}}" selected>{{$data->faculty}}</option>
                                      @endforeach--}}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="example-email">Credits</label>
                        <input type="number" id="credits" name="credits" class="form-control" placeholder="credits" required>
                    </div>
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Department</label>
                                <select name="department_id" id="department_id" class="form-control" required>
                                    @foreach($departments as $data)
                                        <option value="{{$data->id}}">{{$data->department}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Degree Program</label>
                                <select name="degree_program_id" id="degree_program_id" class="form-control" required>
                                    {{--@foreach($department as $data)
                                        <option value="{{$data->id}}">{{$data->department}}</option>
                                    @endforeach--}}
                                </select>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <button type="submit" class="btn waves-effect waves-light btn-primary">Save</button>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Course successfully added");
        </script>

    @elseif(session()->has('error'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        </script>
    @endif
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            loadSemesters();

            $id=$("#department_id option:selected").val();
            loadDegreeProgram($id);

            //load semesters
            function loadSemesters(){
                $.ajax(
                    {
                        url: "/get-semesters",
                        type: 'GET',
                        dataType: "JSON",
                        data: {

                        },
                        success: function (response)
                        {
                            console.log(response);
                            if(response['status']=="true"){
                                $.each(response.semesters, function (){
                                    $("#semester").append($("<option     />").val(this.id).text(this.semester));
                                });
                            }

                        }
                    });
            }
            //load degree programme
            function loadDegreeProgram($id){
                $.ajax(
                    {
                        url: "/get-degree-program/"+$id,
                        type: 'GET',
                        dataType: "JSON",
                        data: {

                        },
                        success: function (response)
                        {
                            console.log(response);
                            if(response['status']=="true"){
                                $.each(response.degreePrograms, function (){
                                    $("#degree_program_id").append($("<option     />").val(this.id).text(this.degree_program));
                                });
                            }

                        }
                    });
            }

            $("#year").change(function(){
                $('#semester').empty();
                loadSemesters();

            });
            $("#department_id").change(function(){
                $('#degree_program_id').empty();
                $id=$("#department_id option:selected").val();
                loadDegreeProgram($id);

            });
        });
    </script>
@endsection
