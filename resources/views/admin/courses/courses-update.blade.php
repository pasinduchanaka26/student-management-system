@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Update Courses</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Courses</li>
                <li class="breadcrumb-item active">Edit Courses</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-40" action="{{Route('courses.update',$courses->id)}}" method="post">
                    {{csrf_field()}}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="example-email">Course Name</label>
                        <input type="text" id="course" name="course" value="{{$courses->course}}" class="form-control" placeholder="Course Name" required>
                    </div>
                    <div class="form-group">
                        <label for="example-email">Course Code</label>
                        <input type="text" id="course_code" value="{{$courses->course_code}}" name="course_code" class="form-control" placeholder="Course Code" required>
                    </div>
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Year</label>
                                <select name="year" id="year" class="form-control" required>
                                    @foreach($year as $data)
                                        <option value="{{$data->id}}" @php if($data->id==$courses->year)echo "selected" @endphp>{{$data->year}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Semester</label>
                                <select name="semester" id="semester" class="form-control" required>
                                    @foreach($semester as $data)
                                        <option value="{{$data->id}}" @php if($data->id==$courses->semester)echo "selected" @endphp>{{$data->semester}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="example-email">Credits</label>
                        <input type="number" id="credits" name="credits" value="{{$courses->credits}}" class="form-control" placeholder="credits" required>
                    </div>
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Department</label>
                                <select name="department_id" id="department_id" class="form-control" required>
                                    @foreach($departments as $data)
                                        <option value="{{$data->id}}" @php if($data->id==$courses->department_id)echo "selected" @endphp>{{$data->department}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Degree Program</label>
                                <select name="degree_program_id" id="degree_program_id" class="form-control" required>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="is_active" id="is_active" class="form-control">
                            <option value="1" @php if($courses->is_active==1)echo "selected" @endphp >Active</option>
                            <option value="0" @php if($courses->is_active==0)echo "selected" @endphp >De-Active</option>

                        </select>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <button type="submit" class="btn waves-effect waves-light btn-primary">Update</button>
                        <a href="{{Route('courses.create')}}" class="btn waves-effect waves-light btn-success">Courses list</a>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Course successfully updated");
        </script>

    @elseif(session()->has('error'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        </script>
    @endif
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $id=$("#department_id option:selected").val();
            loadDegreeProgram($id);
            //load degree programme
            function loadDegreeProgram($id){
                $.ajax(
                    {
                        url: "/get-degree-program/"+$id,
                        type: 'GET',
                        dataType: "JSON",
                        data: {

                        },
                        success: function (response)
                        {
                            console.log(response);
                            if(response['status']=="true"){
                                $.each(response.degreePrograms, function (){
                                    $("#degree_program_id").append($("<option     />").val(this.id).text(this.degree_program));
                                });
                            }

                        }
                    });
            }
            $("#department_id").change(function(){
                $('#degree_program_id').empty();
                $id=$("#department_id option:selected").val();
                loadDegreeProgram($id);

            });
        })


    </script>
@endsection
