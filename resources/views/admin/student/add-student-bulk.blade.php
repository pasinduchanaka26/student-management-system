@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Student Quick Registration</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Student</li>
                <li class="breadcrumb-item active">Student Bulk Registration</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card ">
                <div class="card-body">
                    <form action="{{Route('student-bulk-import')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Reg Number</label>
                                        <div class="col-md-9">
                                            <input type="file" id="student_bulk" name="student_bulk" class="form-control"required>
                                            {{--<small class="form-control-feedback"> This is inline help </small>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-success">Upload</button>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"> </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- /.row -->
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Excel Data Imported successfully.");
        </script>

    @elseif(session()->has('error'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        </script>
    @elseif(session()->has('duplicte'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Student already exsits!!',
            })
        </script>
    @endif
@endsection
