@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">View Students</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Department</li>
                <li class="breadcrumb-item active">View Students</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="control-label text-right col-md-3">Batch</label>
                <div class="col-md-9">
                    <select name="batch_id" id="batch" class="form-control" required>
                        <option value="0">Select Batch</option>
                        @foreach($batch as $data)
                            <option value="{{$data->id}}">{{$data->batch}}</option>
                        @endforeach
                    </select>
                    {{--<small class="form-control-feedback"> Select your gender. </small> --}}
                </div>
            </div>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>EP No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Department</th>
                                <th>Result</th>

                            </tr>
                            </thead>
                            <tbody>

                            {{--@foreach($departements as $departement )
                                <tr>
                                    <td>{{$departement->department}}</td>
                                    <td>{{$departement->getFaculty['faculty']}}</td>
                                    <td>
                                        @if($departement->is_active==1)
                                            <div class="label label-table label-success">Active</div>
                                        @else
                                            <div class="label label-table label-danger">De-Active</div>
                                        @endif
                                    </td>
                                    <td>{{$departement->created_at}}</td>
                                    <td>
                                        <a class="btn waves-effect waves-light btn-sm btn-info" href={{ Route('department.edit',$departement->id) }}><i class="ti-settings"></i></a>
                                        <button data-id="{{$departement->id}}" data-token="{{ csrf_token() }}" class="btn waves-effect waves-light btn-sm btn-danger btnDelete"><i class="ti-trash"></i></button>
                                    </td>

                                </tr>
                            @endforeach--}}

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script>
        $(document).ready(function(){
            loadStudents(0);
            //load student details
            function loadStudents($id){
                $.ajax(
                    {
                        url: "/get-students/"+$id,
                        type: 'GET',
                        dataType: "JSON",
                        data: {

                        },
                        success: function (response)
                        {
                            console.log(response);
                            if(response['status']=="true"){
                                var trHTML = '';
                                $.each(response.students, function (){
                                    trHTML += '<tr class="tblRows">' +

                                        '<td><a href="{{url('student-dashboard')}}'+'/'+ this.id+'" target="_blank"><img src="{{asset('profile_pictures')}}'+'/'+ this.profile_picture+'" alt="user" width="40" class="img-circle" />' + this.registration_number+'</a>'+
                                        '</td>'+
                                        '<td>' + this.first_name + '</td><td>' + this.last_name + '</td>'

                                        +'<td>' + this.department + '</td><td>'+'<button type="button" id="'+this.id+'" class="btn waves-effect waves-light btn-rounded btn-xs btn-info">View</button>'+'</td></tr>';
                                    //$("#degree_program_id").append($("<option     />").val(this.id).text(this.degree_program));
                                });
                                $('#myTable').append(trHTML);
                                loadTable();
                            }

                        }
                    });
            }
            $("#batch").change(function(){
                $(".tblRows").empty();
                $id=$("#batch option:selected").val();
                loadStudents($id);

            });

        });
        //load table
        function loadTable(){
            $(function () {
                $('#myTable').DataTable();
                $(function () {
                    var table = $('#example').DataTable({
                        "columnDefs": [{
                            "visible": false,
                            "targets": 2
                        }],
                        "order": [
                            [2, 'asc']
                        ],
                        "displayLength": 25,
                        "drawCallback": function (settings) {
                            var api = this.api();
                            var rows = api.rows({
                                page: 'current'
                            }).nodes();
                            var last = null;
                            api.column(2, {
                                page: 'current'
                            }).data().each(function (group, i) {
                                if (last !== group) {
                                    $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                    last = group;
                                }
                            });
                        }
                    });
                    // Order by the grouping
                    $('#example tbody').on('click', 'tr.group', function () {
                        var currentOrder = table.order()[0];
                        if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                            table.order([2, 'desc']).draw();
                        } else {
                            table.order([2, 'asc']).draw();
                        }
                    });
                });
            });
        }






    </script>
@endsection
