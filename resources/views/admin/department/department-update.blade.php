@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Update Department</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Department</li>
                <li class="breadcrumb-item active">Edit Department</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-40" action="{{Route('department.update',$department->id)}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="example-email">Department Name</label>
                        <input type="text" id="departmentName" name="department" value="{{$department->department}}" class="form-control" placeholder="Department Name" required>
                    </div>
                    <div class="form-group">
                        <label>Faculty</label>
                        <select name="faculty_id" id="faculty" class="form-control" readonly="true">
                            @foreach($faculty as $data)
                                <option value="{{$data->id}}" @php if($data->id==$department->id)echo "selected" @endphp >{{$data->faculty}}</option>
                            @endforeach
                        </select>
                        {{--                        <input type="text" id="faculty" value="{{$faculty->faculty}}" name="faculty_id" class="form-control" placeholder="Faculty" required disabled>--}}
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="is_active" id="is_active" class="form-control">
                            <option value="1" @php if($department->is_active==1)echo "selected" @endphp >Active</option>
                            <option value="0" @php if($department->is_active==0)echo "selected" @endphp >De-Active</option>

                        </select>
                        {{--                        <input type="text" id="faculty" value="{{$faculty->faculty}}" name="faculty_id" class="form-control" placeholder="Faculty" required disabled>--}}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn waves-effect waves-light btn-primary">Update</button>
                        <a href="{{Route('department.create')}}" class="btn waves-effect waves-light btn-success">Department list</a>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Department successfully updated");
        </script>

    @elseif(session()->has('error'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        </script>
    @endif
@endsection
