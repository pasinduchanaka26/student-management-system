@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Add Department</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Department</li>
                <li class="breadcrumb-item active">Add Department</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-40" action="{{Route('department.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="example-email">Department Name</label>
                        <input type="text" id="departmentName" name="department" class="form-control" placeholder="Department Name" required>
                    </div>
                    <div class="form-group">
                        <label>Faculty</label>
                        <select name="faculty_id" id="faculty" class="form-control" readonly="true">
                            @foreach($faculty as $data)
                                <option value="{{$data->id}}" selected>{{$data->faculty}}</option>
                            @endforeach
                        </select>
{{--                        <input type="text" id="faculty" value="{{$faculty->faculty}}" name="faculty_id" class="form-control" placeholder="Faculty" required disabled>--}}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn waves-effect waves-light btn-primary">Save</button>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Department successfully added");
        </script>

    @elseif(session()->has('error'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        </script>
    @endif
@endsection
