@extends('admin.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">View Users</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Users</li>
                <li class="breadcrumb-item active">View Users</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Mobile Number</th>
                               {{-- <th>Land Phone</th>--}}
                                <th>Email</th>
                                <th>User Type</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $user )
                                <tr>
                                    <td>{{$user->first_name}}</td>
                                    <td>{{$user->last_name}}</td>
                                    <td>{{$user->mobile_phone}}</td>
                                    {{--<td>{{$user->land_phone}}</td>--}}
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->roll}}</td>
                                    <td>
                                        @if($user->is_active==1)
                                            <div class="label label-table label-success">Active</div>
                                        @else
                                            <div class="label label-table label-danger">De-Active</div>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn waves-effect waves-light btn-sm btn-info" href={{ Route('users.edit',$user->userId) }}><i class="ti-settings"></i></a>
                                        <button data-id="{{$user->userId}}" data-token="{{ csrf_token() }}" class="btn waves-effect waves-light btn-sm btn-danger btnDelete"><i class="ti-trash"></i></button>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script>
        $(function () {
            $('#myTable').DataTable();
            $(function () {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function (settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function (group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function () {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        //delete button click

        $(document).ready(function(){
            $(".btnDelete").click(function(){
                //Warning Message

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {

                        var id = $(this).data("id");
                        var token = $(this).data("token");

                        $.ajax(
                            {
                                url: "/users/"+id,
                                type: 'DELETE',
                                dataType: "JSON",
                                data: {
                                    "id": id,
                                    "_method": 'DELETE',
                                    "_token": token,
                                },
                                success: function (response)
                                {
                                    console.log(response);
                                    if(response['status']=="true"){
                                        Swal.fire(
                                            'Deleted!',
                                            'User has been deleted.',
                                            'success'
                                        )
                                        setTimeout(function(){
                                            location.reload();
                                        }, 2000)
                                    }else{
                                        Swal.fire({
                                            type: 'error',
                                            title: 'Oops...',
                                            text: 'Something went wrong!',
                                        })
                                    }

                                }
                            });



                    }
                })

            });
        });

    </script>
@endsection
