@extends('student.layouts.app')

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Self Registration Form</h3>
        </div>
        {{--<div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">Degree Program</li>
                <li class="breadcrumb-item active">Add Degree Program</li>
            </ol>
        </div>--}}
        {{--<div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>--}}
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <form class="form-horizontal m-t-40" action="{{Route('student-dashboard.store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Profile Picture</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-secondary btn-file">
                                            <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="image">
                                        </span>
                                    <a href="#" class="input-group-addon btn btn-secondary fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-email">Department</label>
                                <select name="department_id" id="department_id" class="form-control" required>
                                    <option value="">Select Department</option>
                                    @foreach($departements as $data)
                                        <option value="{{$data->id}}">{{$data->department}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-email">Degree Program</label>
                                <select name="degree_id" id="degree_id" class="form-control" required>
                                    <option value="">Select</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="example-email">Name with Initials</label>
                                <input type="text" id="name_with_initials" name="name_with_initial" class="form-control" placeholder="Eg: A.B.C Perera" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-email">Registration Number</label>
                                <input type="text" id="reg_number" value="{{$studentDetails->registration_number}}" class="form-control" required disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label>First Name</label>
                                <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name" required>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" required>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-email">Full Name</label>
                                <input type="text" id="full_name" name="full_name" class="form-control" placeholder="Full Name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label>Nationality</label>
                                <select name="nationality" id="nationality" class="form-control" required>
                                    <option value="Sri Lankan">Sri Lankan</option>
                                </select>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Race</label>
                                <select name="race" id="race" class="form-control" required>
                                    <option value="Sinhalease">Sinhalease</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Religion</label>
                                <select name="religion" id="religion" class="form-control" required>
                                    <option value="Buddhism">Buddhism</option>
                                </select>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-email">Date of Birth</label>
                                <input type="date" id="birth_date" name="birth_date" class="form-control" placeholder="dd/mm/yyyy" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-email">NIC</label>
                                <input type="text" id="nic" name="nic" class="form-control" placeholder="Eg: 962548769v" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contact Address</label>
                                <textarea id="contact_address" name="contact_address" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Permanent Address</label>
                                <textarea id="permanent_address" name="permanent_address" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label>Province</label>
                                <input type="text" id="province" name="province" class="form-control" required>
                                {{--<select name="province" id="province" class="form-control" required>
                                    <option value="Sri Lankan">Sri Lankan</option>
                                </select>--}}
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>District</label>
                                {{--<select name="district" id="district" class="form-control" required>
                                    <option value="Sinhalease">Sinhalease</option>
                                </select>--}}
                                <input type="text" id="district" name="district" class="form-control" required>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
{{--                    <div class="row">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="example-email">Full Name</label>--}}
{{--                                <input type="text" id="full_name" name="full_name" class="form-control" placeholder="Full Name" required>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label>Phone(Mobile)</label>
                                <input type="number" id="mobile_phone" name="mobile_phone" class="form-control" placeholder="Eg: 0777123123" required>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Phone(Landline)</label>
                                <input type="number" id="land_phone" name="land_phone" class="form-control" placeholder="Eg: 0342241587" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" id="email" name="email" value="{{$studentDetails->email}}" class="form-control" placeholder="Eg: student@gmail.com" required>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label>Guardian Name</label>
                                <input type="text" id="guardian_name" name="guardian_name" class="form-control" placeholder="Your guardian's name" required>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Guardian Contact</label>
                                <input type="number" id="guardian_telephone" name="guardian_telephone" class="form-control" placeholder="Eg: 0777123123" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Guardian Address</label>
                                <textarea id="guardian_address" name="guardian_address" placeholder="Your guardian's address" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="{{$studentDetails->id}}">
                        <!--/span-->
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <button type="submit" class="btn waves-effect waves-light btn-primary">Save</button>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection
@section('script')
    <script>
        $(document).ready(function(){

           /* $id=$("#department_id option:selected").val();
            loadDegreeProgram($id);*/

            //load degree programme
            function loadDegreeProgram($id){
                $.ajax(
                    {
                        url: "/get-degree-program/"+$id,
                        type: 'GET',
                        dataType: "JSON",
                        data: {

                        },
                        success: function (response)
                        {
                            console.log(response);
                            if(response['status']=="true"){
                                $.each(response.degreePrograms, function (){
                                    $("#degree_id").append($("<option     />").val(this.id).text(this.degree_program));
                                });
                            }

                        }
                    });
            }

            $("#department_id").change(function(){
                $('#degree_id').empty();
                $id=$("#department_id option:selected").val();
                loadDegreeProgram($id);

            });
        });
    </script>
@endsection
