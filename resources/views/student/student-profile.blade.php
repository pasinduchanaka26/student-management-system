@extends($view)
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Profile</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">pages</li>
                <li class="breadcrumb-item active">Profile</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body">
                    <center class="m-t-30"> <img src="{{asset('profile_pictures/'.$student->profile_picture)}}" class="img-circle" width="150" />
                        <h4 class="card-title m-t-10">{{$student->full_name}}</h4>
                        <h6 class="card-subtitle">{{$student->getDegreeprogram->degree_program}}</h6>
                        <h2 class="card-subtitle">{{$student->getDepartment->department}}</h2>
                        <div class="row text-center justify-content-md-center">
                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                        </div>
                    </center>
                </div>
                <div>
                    <hr> </div>
                <div class="card-body">
                    <small class="text-muted">Registration Number</small>
                    <h4>{{$student->registration_number}}</h4>
                    <small class="text-muted">NIC</small>
                    <h6>{{$student->nic}}</h6>
                    <small class="text-muted">Full Name</small>
                    <h6>{{$student->full_name}}</h6>
                    <small class="text-muted">Email address </small>
                    <h6>{{$student->email}}</h6>
                    <small class="text-muted p-t-30 db">Mobile Phone</small>
                    <h6>{{$student->mobile_phone}}</h6>
                    <small class="text-muted p-t-30 db">Land Phone</small>
                    <h6>{{$student->land_phone}}</h6>
                    <small class="text-muted p-t-30 db">Address</small>
                    <h6>{{$student->permanent_address}}</h6>
                    {{--<div class="map-box">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>--}}
                    <small class="text-muted p-t-30 db">Social Profile</small>
                    <br/>
                    <button class="btn btn-circle btn-secondary"><i class="fab fa-facebook-f"></i></button>
                    <button class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></button>
                    <button class="btn btn-circle btn-secondary"><i class="fab fa-youtube"></i></button>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs profile-tab" role="tablist">
                   {{-- <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Timeline</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>--}}
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="settings" role="tabpanel">
                        <div class="card-body">
                            <form class="form-horizontal form-material" action="{{Route('student-dashboard.update',$student->id)}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{ method_field('PATCH') }}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-12">Registration Number</label>
                                    <div class="col-md-12">
                                        <input type="text" id="reg_number" value="{{$student->registration_number}}" class="form-control form-control-line" required disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Department</label>
                                    <div class="col-sm-12">
                                        <select name="department_id" id="department_id" class="form-control form-control-line" required>
                                            <option value="">Select Department</option>
                                            @foreach($departements as $data)
                                                <option value="{{$data->id}}" <?php if($student->department_id == $data->id) echo "selected";?>>{{$data->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Degree Program</label>
                                    <div class="col-sm-12">
                                        <select name="degree_id" id="degree_id" class="form-control form-control-line" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Name with Initials</label>
                                    <div class="col-md-12">
                                        <input type="text" id="name_with_initials" value="{{$student->name_with_initial}}" name="name_with_initial" class="form-control form-control-line" placeholder="Eg: A.B.C Perera" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">First Name</label>
                                    <div class="col-md-12">
                                        <input type="text" value="{{$student->first_name}}" id="first_name" name="first_name" class="form-control form-control-line" placeholder="First Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Last Name</label>
                                    <div class="col-md-12">
                                        <input type="text" value="{{$student->last_name}}" id="last_name" name="last_name" class="form-control form-control-line" placeholder="Last Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Full Name</label>
                                    <div class="col-md-12">
                                        <input type="text" value="{{$student->full_name}}" id="full_name" name="full_name" class="form-control form-control-line" placeholder="Full Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Nationality</label>
                                    <div class="col-md-12">
                                        <select name="nationality" id="nationality" class="form-control form-control-line" required>
                                            <option value="Sri Lankan">Sri Lankan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Race</label>
                                    <div class="col-md-12">
                                        <select name="race" id="race" class="form-control form-control-line" required>
                                            <option value="Sinhalease">Sinhalease</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Religion</label>
                                    <div class="col-md-12">
                                        <select name="religion" id="religion" class="form-control form-control-line" required>
                                            <option value="Buddhism">Buddhism</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Date of Birth</label>
                                    <div class="col-md-12">
                                        <input type="date" id="birth_date" value="{{$student->birth_date}}"  name="birth_date" class="form-control form-control-line" placeholder="dd/mm/yyyy" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">NIC</label>
                                    <div class="col-md-12">
                                        <input type="text" id="nic" name="nic" value="{{$student->nic}}" class="form-control form-control-line" placeholder="Eg: 962548769v" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Contact Address</label>
                                    <div class="col-md-12">
                                        <textarea id="contact_address" name="contact_address" class="form-control form-control-line" rows="5">{{$student->contact_address}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Permanent Address</label>
                                    <div class="col-md-12">
                                        <textarea id="permanent_address" name="permanent_address" class="form-control form-control-line" rows="5">{{$student->permanent_address}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Province</label>
                                    <div class="col-md-12">
                                        <input type="text" value="{{$student->province}}" id="province" name="province" class="form-control form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">District</label>
                                    <div class="col-md-12">
                                        <input type="text" value="{{$student->district}}" id="district" name="district" class="form-control form-control-line" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone(Mobile)</label>
                                    <div class="col-md-12">
                                        <input type="number" value="{{$student->mobile_phone}}" id="mobile_phone" name="mobile_phone" class="form-control form-control-line" placeholder="Eg: 0777123123" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone(Landline)</label>
                                    <div class="col-md-12">
                                        <input type="number" value="{{$student->land_phone}}" id="land_phone" name="land_phone" class="form-control form-control-line" placeholder="Eg: 0342241587" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" id="email" name="email" value="{{$student->email}}" class="form-control form-control-line" placeholder="Eg: student@gmail.com" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Guardian Name</label>
                                    <div class="col-md-12">
                                        <input type="text" id="guardian_name" value="{{$student->guardian_name}}" name="guardian_name" class="form-control form-control-line" placeholder="Your guardian's name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Guardian Contact</label>
                                    <div class="col-md-12">
                                        <input type="number" id="guardian_telephone" value="{{$student->guardian_telephone}}" name="guardian_telephone" class="form-control form-control-line" placeholder="Eg: 0777123123" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Guardian Address</label>
                                    <div class="col-md-12">
                                        <textarea id="guardian_address" name="guardian_address" class="form-control form-control-line" rows="3">{{$student->guardian_address}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Profile Picture</label>
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-secondary btn-file">
                                        <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="image">
                                    </span>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit"  class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    @if(session()->has('info'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire("Student details successfully updated");
        </script>

    @elseif(session()->has('error'))
        <script src="{{asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
        <script src="{{asset('assets/plugins/sweetalert2/sweet-alert.init.js')}}"></script>
        <script>

            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        </script>
    @endif
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
@endsection
@section('script')
    <script>
        $(document).ready(function(){

             $id=$("#department_id option:selected").val();
             loadDegreeProgram($id);

            //load degree programme
            function loadDegreeProgram($id){
                $.ajax(
                    {
                        url: "/get-degree-program/"+$id,
                        type: 'GET',
                        dataType: "JSON",
                        data: {

                        },
                        success: function (response)
                        {
                            console.log(response);
                            if(response['status']=="true"){
                                $.each(response.degreePrograms, function (){
                                    $("#degree_id").append($("<option     />").val(this.id).text(this.degree_program));
                                });
                            }

                        }
                    });
            }

            $("#department_id").change(function(){
                $('#degree_id').empty();
                $id=$("#department_id option:selected").val();
                loadDegreeProgram($id);

            });
        });
    </script>
@endsection
