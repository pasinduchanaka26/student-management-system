<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">

<title>Student Management System</title>
<link rel="canonical" href="https://www.wrappixel.com/templates/adminpro/" />
<!-- Bootstrap Core CSS -->
<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/perfect-scrollbar/dist/css/perfect-scrollbar.min.css')}}" rel="stylesheet">
<!-- This page CSS -->
<!-- chartist CSS -->
<link href="{{asset('assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
<!--c3 CSS -->
<link href="{{asset('assets/plugins/c3-master/c3.min.css')}}" rel="stylesheet">
<!--Toaster Popup message CSS -->
<link href="{{asset('assets/plugins/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
<!-- Dashboard 1 Page CSS -->
<link href="{{asset('assets/css/pages/dashboard1.css')}}" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="{{asset('assets/css/colors/megna-dark.css')}}" id="theme" rel="stylesheet">
<!-- Data table styles -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
