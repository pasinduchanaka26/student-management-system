<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('batch_id');
            $table->integer('department_id');
            $table->integer('degree_id');
            $table->text('registration_number');
            $table->text('name_with_initial');
            $table->text('first_name');
            $table->text('last_name');
            $table->text('full_name');
            $table->text('nationality');
            $table->text('race');
            $table->text('religion');
            $table->date('birth_date');
            $table->text('nic');
            $table->text('contact_address');
            $table->text('permanent_address');
            $table->text('district');
            $table->text('province');
            $table->integer('mobile_phone');
            $table->integer('land_phone');
            $table->text('email');
            $table->text('guardian_name');
            $table->integer('guardian_telephone');
            $table->text('guardian_address');



            $table->string('added_by');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
